function submit(){

    var author = $("input[name=newauthor").val();   
    var title = $("input[name=newtitle]").val();
    var body = $("input[name=newbody]").val();

    $.ajax({
        type: "POST",
        url: "blog.php",
        data: {
            "author":author,
            "title":title,
            "body":body
        },
        success: function (data){
            processData(data);
        },
        failure: function (data){
            alert("fail!");
        }
    });

}

function processData(data){
    var postsToBeAdded = "";
    var count = 0;
    //0 is author
    //1 is title
    //2 is body
    //3 is the beginning of a new blog post.
    
    for(var i = 0; i < data.length; i++){
        if(data[i] == "|"){
            postsToBeAdded += "<br><br>";
            count += 1;
            if (count == 3){
                postsToBeAdded += "----------------------------------------------------------------<br>";
                count = 0;
            }
        }else{   
            postsToBeAdded += data[i];
        }
    }

    $("#blogposts").html(postsToBeAdded);   
}

//Get the contents "database" if it is empty, do nothing. Otherwise, update the div "blogposts" with the data.

$.ajax({
    type: "GET",
    url: "blog.php",
    success: function (data){
        if(data.length == 0){
            console.log("no data!");
        }else{
            processData(data);
        }
    }
});